package network.elekmonar.classifier.commons.enums;

public enum Multiplicity {

	SINGLE,
	
	MULTI;
	
}
