package network.elekmonar.classifier.commons;

import java.io.Serializable;

public class CacheBean implements Serializable {

	private static final long serialVersionUID = 5156837693384879389L;
	
	private Long id;
	
	private String naturalId;
	
	private String name;
	
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaturalId() {
		return naturalId;
	}

	public void setNaturalId(String naturalId) {
		this.naturalId = naturalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}