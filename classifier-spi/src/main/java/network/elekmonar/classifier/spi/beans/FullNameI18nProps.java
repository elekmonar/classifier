package network.elekmonar.classifier.spi.beans;

import javax.xml.bind.annotation.XmlElement;

public class FullNameI18nProps extends I18nProps {
	
	private String fullName;

	@XmlElement(name = "fullName", required = true)
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

}