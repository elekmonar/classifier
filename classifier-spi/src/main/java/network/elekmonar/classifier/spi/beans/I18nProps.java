package network.elekmonar.classifier.spi.beans;

import javax.xml.bind.annotation.XmlElement;

public class I18nProps {

	private String name;
	
	private String description;
	
	private String property1;
	
	private String property2;
	
	private String property3;

	@XmlElement(name = "name", required = true)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "description", required = false)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement(name = "property1", required = false)
	public String getProperty1() {
		return property1;
	}

	public void setProperty1(String property1) {
		this.property1 = property1;
	}

	@XmlElement(name = "property2", required = false)
	public String getProperty2() {
		return property2;
	}

	public void setProperty2(String property2) {
		this.property2 = property2;
	}

	@XmlElement(name = "property3", required = false)
	public String getProperty3() {
		return property3;
	}

	public void setProperty3(String property3) {
		this.property3 = property3;
	}
	
}