package network.elekmonar.classifier.spi.transformer;

import network.elekmonar.classifier.spi.beans.ClassifierRB;
import network.elekmonar.classifier.spi.persistence.Classifier;

public abstract class RepresentationTransformer<T extends Classifier, E extends ClassifierRB> {
	
	public abstract E newRepresentationBean();
	
	public abstract void transform(T entity, E dto);
	
	public E doTransform(T entity) {
		E dto = newRepresentationBean();
		dto.setId(entity.getId());
		dto.setNaturalId(entity.getNaturalId());
		dto.setName(entity.getName());
		dto.setDescription(entity.getDescription());			
				
		transform(entity, dto);
		
		return dto;
	}

}