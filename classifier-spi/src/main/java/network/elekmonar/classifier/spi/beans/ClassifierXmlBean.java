package network.elekmonar.classifier.spi.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import network.elekmonar.commons.spec.able.NaturalIdable;

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlSeeAlso({SimpleClassifierXmlBean.class, ClassifierI18nXmlBean.class})
public class ClassifierXmlBean implements NaturalIdable, Serializable {

	private static final long serialVersionUID = 1619471984363691668L;
	
	private Long id;
	
	private String naturalId;
		
	@XmlElement(name = "id", required = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlElement(name = "naturalId", required = true)
	public String getNaturalId() {
		return naturalId;
	}

	public void setNaturalId(String naturalId) {
		this.naturalId = naturalId;
	}

}