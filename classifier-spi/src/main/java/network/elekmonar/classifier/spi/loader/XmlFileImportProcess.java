package network.elekmonar.classifier.spi.loader;

import javax.persistence.EntityManager;

import network.elekmonar.classifier.spi.beans.ClassifierXmlBean;
import network.elekmonar.classifier.spi.beans.I18nProps;
import network.elekmonar.commons.spec.able.NaturalIdable;

public interface XmlFileImportProcess<T extends NaturalIdable, E extends ClassifierXmlBean> {
	
	public void doImport(EntityManager em, T instance, E xmlBean);
	
	public void doImportI18n(EntityManager em, T instance, I18nProps i18nProps);

}