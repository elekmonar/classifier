package network.elekmonar.classifier.spi;

import java.util.List;

public interface XmlBeanTypesProvider {
	
	List<Class<?>> getXmlBeans();

}