package network.elekmonar.classifier.spi.process;

import javax.persistence.EntityManager;

import network.elekmonar.commons.spec.enums.Language;

public interface ClassifierItemListImport {

	public void firstPass(EntityManager em, Language language);
	
	public void secondPass(EntityManager em, Language language);
	
}