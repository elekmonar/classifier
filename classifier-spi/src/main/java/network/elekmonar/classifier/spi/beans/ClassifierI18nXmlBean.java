package network.elekmonar.classifier.spi.beans;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlSeeAlso;

//import network.elekmonar.classifier.loader.beans.FrameworkXmlBean;
//import network.elekmonar.classifier.loader.beans.InternetResourceXmlBean;

@XmlAccessorType(XmlAccessType.PROPERTY)
//@XmlSeeAlso({InternetResourceXmlBean.class, FrameworkXmlBean.class})
public class ClassifierI18nXmlBean extends ClassifierXmlBean {

	private static final long serialVersionUID = 3134975068382755385L;

	private Map<String, I18nProps> i18n;
		
//	@XmlJavaTypeAdapter(NameDescAdapter.class)
	@XmlElementWrapper(name = "i18n")
	public Map<String, I18nProps> getI18n() {
		if (i18n == null) {
			i18n = new HashMap<>();
		}
		
		return i18n;
	}

	public void setI18n(Map<String, I18nProps> i18n) {
		this.i18n = i18n;
	}
	
}