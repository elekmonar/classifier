package network.elekmonar.classifier.spi.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;

//import network.elekmonar.classifier.loader.beans.AdministrativeDivisionXmlBean;
//import network.elekmonar.classifier.loader.beans.ContinentXmlBean;
//import network.elekmonar.classifier.loader.beans.CountryXmlBean;
//import network.elekmonar.classifier.loader.beans.ArtGenreXmlBean;

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlSeeAlso({})
//@XmlSeeAlso({CountryXmlBean.class, ContinentXmlBean.class, AdministrativeDivisionXmlBean.class, ArtGenreXmlBean.class})
public class SimpleClassifierXmlBean extends ClassifierXmlBean {
	
	private static final long serialVersionUID = 414479190954175629L;

	private String name;
	
	private String description;
	
	@XmlElement(name = "name", required = true)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "description", required = false)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}