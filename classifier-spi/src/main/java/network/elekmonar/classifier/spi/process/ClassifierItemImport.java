package network.elekmonar.classifier.spi.process;

import javax.persistence.EntityManager;

import network.elekmonar.commons.spec.able.NaturalIdable;

public interface ClassifierItemImport<T extends NaturalIdable, E>  {

	public void doImport(EntityManager em, T instance, E bean);
	
}