package network.elekmonar.classifier.spi.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.NaturalId;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Store;

import network.elekmonar.commons.spec.able.NaturalIdable;

@DynamicUpdate
@MappedSuperclass
public class Classifier implements NaturalIdable, Serializable {

	private static final long serialVersionUID = 8962857702515804586L;
	
	/**
	 * Идентификатор
	 */
	private Long id;
	
	/**
	 * Natural ID
	 */
	private String naturalId;
	
	/**
	 * Контрольная сумма (хэш из значений полей)
	 */
	private String checksum;

	/**
	 * Название/наименование
	 */
	private String name;
	
	/**
	 * Описание
	 */
	private String description;	
		
	@Id
	@GeneratedValue(generator = "classifier_id_generator")
	@Column(name = "id")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@NaturalId
	@Column(name = "natural_id", length = 32, nullable = true)	
	public String getNaturalId() {
		return naturalId;
	}

	public void setNaturalId(String naturalId) {
		this.naturalId = naturalId;
	}
	
	@Column(name = "checksum", length = 64, nullable = false)
	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	
	@Column(name = "name", length = 64, nullable = false)
	@Field(name = "name", index = Index.YES, analyze = Analyze.YES, store = Store.YES)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "description", length = 1024, nullable = true)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
    public int hashCode() {
        int result = 29;
        result += 29 * (getId() != null ? getId().hashCode() : 0);
        return result;
    }
	
}