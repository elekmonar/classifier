package network.elekmonar.classifier.geonames;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.jboss.jca.adapters.jdbc.jdk8.WrappedConnectionJDK8;
import org.postgis.PGgeometry;
import org.postgis.Point;
import org.postgresql.PGConnection;

import network.elekmonar.commons.utils.ChecksumHelper;
import network.elekmonar.modular.spec.EJBLookupPaths;
import network.elekmonar.modular.spec.identifier.ClassifierIdentifierManager;
import network.elekmonar.modular.spec.metamodel.MetamodelProvider;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class GeonamesImportProcess implements Serializable {

	private static final long serialVersionUID = -3608858056797422597L;
	
	private static final String ALL_COUNTRIES_IDS_QUERY = "select geonameid from all_countries where feature_code ~ '^PPL' order by geonameid";
	
	private static final String ALL_COUNTRIES_BY_ID_QUERY = "select geonameid, name, latitude, longitude, feature_class, feature_code, country_code, population  from all_countries where geonameid = ?";
	
	private static final String COUNTRIES_MAP_QUERY = "select alpha2, id from classifier.country where alpha2 is not null";
	
	private static final String POPULATED_LOCALITY_INSERT = "insert into classifier.populated_locality(id, checksum, name, name_i18n_en, country_id, location) values(?, ?, ?, ?, ?, ?)";
	
	private static final String POPULATED_LOCALITY_I18N = "select alternate_name from alternate_names where geonameid = ? and isolanguage = ?";
	
	private static final String POPULATED_LOCALITY = "PopulatedLocality";
	
	@Resource(lookup = "java:jboss/datasources/geonamesDS")
	private DataSource geonamesDs;

	@Resource(lookup = "java:jboss/datasources/elekmonarDS")
	private DataSource elekmonarDs;
	
//	@EJB(lookup = EJBLookupPaths.METAMODEL_PROVIDER)
	private MetamodelProvider metamodelProvider;
	
//	@EJB(lookup = EJBLookupPaths.CLASSIFIER_IDENTIFIER_MANAGER)
	private ClassifierIdentifierManager classifierIdentifierManager;
	
	@Inject
	private ChecksumHelper checksumHelper;
		
	@Inject
	private Event<ExceptionToCatchEvent> exceptionToCatch;
	
	private Short localityResourceId;
	
	@Asynchronous
	public void doProcess(@Observes GeonamesProcessEvent event) {
		System.out.println("do process ...");
		try {
			Context ctx = new InitialContext();
			metamodelProvider = (MetamodelProvider)ctx.lookup(EJBLookupPaths.METAMODEL_PROVIDER);
			classifierIdentifierManager = (ClassifierIdentifierManager)ctx.lookup(EJBLookupPaths.CLASSIFIER_IDENTIFIER_MANAGER);
		} catch (NamingException e) {
			e.printStackTrace();
			// TODO !!!
			throw new RuntimeException(e);
		}
		
		System.out.println("metamodelProvider = " + metamodelProvider);
		System.out.println("classifierIdentifierManager = " + classifierIdentifierManager);

		try {
			localityResourceId = metamodelProvider.getResourceIdByLabel(POPULATED_LOCALITY);
			System.out.println("localityResourceId = " + localityResourceId);			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		Map<String, Long> countriesMap = new HashMap<>();
				
		try (
				Connection gnConn = geonamesDs.getConnection();
				Connection elekConn = elekmonarDs.getConnection();				
		) {
			PGConnection pgElekConn = elekConn.unwrap(PGConnection.class);
			pgElekConn.addDataType("geometry", PGgeometry.class);
			System.out.println("pgElekConn = " + pgElekConn);
			
			elekConn.setAutoCommit(false);
			
			initCountriesMap(elekConn, countriesMap);
			System.out.println("countriesMap.size =" + countriesMap.size());
			
			List<Integer> ids = getGeonameIds(gnConn);
			System.out.println("ids.size = " + ids.size());
			GnRecord gnRecord;
			for (Integer id : ids) {							
				gnRecord = getGeonameRecord(gnConn, id);
				insertPopulatedLocality(elekConn, gnRecord, countriesMap);
			}
			
			elekConn.commit();
		} catch(SQLException e) {
			e.printStackTrace();			
			exceptionToCatch.fire(new ExceptionToCatchEvent(e));
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("finish ...");
	}
	
	private void initCountriesMap(Connection conn, Map<String, Long> countriesMap) throws SQLException {
		try (
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(COUNTRIES_MAP_QUERY);
		) {
			while (rs.next()) {
				countriesMap.put(rs.getString(1), rs.getLong(2));
			}
		}
	}
	
	private List<Integer> getGeonameIds(Connection conn) throws SQLException {
		List<Integer> ids = new ArrayList<>();
		try (
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(ALL_COUNTRIES_IDS_QUERY);
		) {			
			while (rs.next()) {
				ids.add(rs.getInt(1));					
			}				
		}
		
		return ids;
	}
	
	private GnRecord getGeonameRecord(Connection conn, Integer id) throws SQLException {
		GnRecord gnRecord = new GnRecord();
		try (
				PreparedStatement pstmt = conn.prepareStatement(ALL_COUNTRIES_BY_ID_QUERY);								
		) {
			pstmt.setInt(1, id);
			try (
					ResultSet rs = pstmt.executeQuery();	
			) {
				rs.next();				
				gnRecord.setId(rs.getInt(1));
				gnRecord.setName(rs.getString(2));
				gnRecord.setLatitude(rs.getDouble(3));
				gnRecord.setLongitude(rs.getDouble(4));
				gnRecord.setFeatureClass(rs.getString(5));
				gnRecord.setFeatureCode(rs.getString(6));
				gnRecord.setCountryCode(rs.getString(7));
				gnRecord.setPopulation(rs.getLong(8));
			}			
		}
		
		addLocalityI18nName(conn, gnRecord, "ru");
		addLocalityI18nName(conn, gnRecord, "en");
		
		return gnRecord;
	}
	
	private void insertPopulatedLocality(Connection conn, GnRecord gnRecord, Map<String, Long> countriesMap) throws SQLException {
		if (!countriesMap.containsKey(gnRecord.getCountryCode().toLowerCase())) {
			return;
		}
		
		Long countryId = countriesMap.get(gnRecord.getCountryCode().toLowerCase());		
		Long localityId = classifierIdentifierManager.getClassifierId(localityResourceId, gnRecord.getId());		
		String checksum = checksumHelper.calculate(gnRecord.getName(), gnRecord.getCountryCode(), gnRecord.getFeatureClass(), 
				gnRecord.getFeatureCode(), gnRecord.getLatitude().toString(), gnRecord.getLongitude().toString());
		
		try (
				PreparedStatement pstmt = conn.prepareStatement(POPULATED_LOCALITY_INSERT);								
		) {
			pstmt.setLong(1, localityId);
			pstmt.setString(2, checksum);
//			pstmt.setString(3, checksumHelper.calculate(UUID.randomUUID().toString()).substring(0,31));			
			if (gnRecord.getI18n().containsKey("ru")) {
				pstmt.setString(3, gnRecord.getI18n().get("ru"));	
			} else {
				pstmt.setString(3, gnRecord.getName());
			}
			if (gnRecord.getI18n().containsKey("en")) {
				pstmt.setString(4, gnRecord.getI18n().get("en"));	
			} else {
				pstmt.setString(4, gnRecord.getName());
			}
			
			pstmt.setLong(5, countryId);
			
			Point point = new Point(gnRecord.getLongitude(), gnRecord.getLatitude());
			pstmt.setObject(6, new PGgeometry(point));			
			
			pstmt.executeUpdate();
		}		
	}
	
	private void addLocalityI18nName(Connection conn, GnRecord gnRecord, String lang) throws SQLException {
		try (
				PreparedStatement pstmt = conn.prepareStatement(POPULATED_LOCALITY_I18N);				
		) {	
			pstmt.setInt(1, gnRecord.getId());
			pstmt.setString(2, lang);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				String i18nName = rs.getString(1);
				gnRecord.getI18n().put(lang, i18nName);				
			}			
		}
	}
	
}
