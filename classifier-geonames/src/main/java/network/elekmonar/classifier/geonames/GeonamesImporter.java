package network.elekmonar.classifier.geonames;

import java.io.Serializable;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import network.elekmonar.signal.spec.spi.ProcessLauncher;
import network.elekmonar.signal.spec.spi.ProcessSignalContext;

@Named("geonames-import")
public class GeonamesImporter implements ProcessLauncher, Serializable {
	
	private static final long serialVersionUID = 6992734626939146752L;
	
	@Inject
	private Event<GeonamesProcessEvent> eventSource;

	public void launch(ProcessSignalContext context) {
		System.out.println("...launch ...");
		eventSource.fire(new GeonamesProcessEvent());
	}
	
}
