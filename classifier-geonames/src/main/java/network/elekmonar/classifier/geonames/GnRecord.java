package network.elekmonar.classifier.geonames;

import java.util.HashMap;
import java.util.Map;

public class GnRecord {
	
	private Integer id;
	
	private String name;
	
	private Double latitude;
	
	private Double longitude;
	
	private String featureClass;
	
	private String featureCode;
	
	private String countryCode;
	
	private Long population;
	
	private Map<String, String> i18n;
	
	public GnRecord() {
		i18n = new HashMap<>();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getFeatureClass() {
		return featureClass;
	}

	public void setFeatureClass(String featureClass) {
		this.featureClass = featureClass;
	}

	public String getFeatureCode() {
		return featureCode;
	}

	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Long getPopulation() {
		return population;
	}

	public void setPopulation(Long population) {
		this.population = population;
	}

	public Map<String, String> getI18n() {
		return i18n;
	}

	public void setI18n(Map<String, String> i18n) {
		this.i18n = i18n;
	}
	
}