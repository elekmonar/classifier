package network.elekmonar.classifier.loader.starter;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.Map;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.apache.deltaspike.core.util.metadata.AnnotationInstanceProvider;

import network.elekmonar.classifier.spi.process.ClassifierItemListImport;
import network.elekmonar.commons.spec.enums.Language;
import network.elekmonar.commons.utils.LanguageResolver;
import network.elekmonar.elefante.jpa.EntityManagerProvider;
import network.elekmonar.signal.spec.ControlSignal;
import network.elekmonar.signal.spec.spi.ProcessLauncher;
import network.elekmonar.signal.spec.spi.ProcessSignalContext;

@Named("classifier-import")
public class ClassifierImportLauncher implements ProcessLauncher, Serializable {

	private static final long serialVersionUID = 5810001479680207420L;
	
	private static final Language DEFAULT_LANG = Language.valueOf("RU");
	
	private static final String IMPORT_TYPE = "type";
	
	private static final String RESOURCE = "resource";

	@Inject
	private EntityManagerProvider emProvider;
	
	@Inject
	private LanguageResolver languageResolver;
	
	private EntityManager em;
	
//	@Inject @Any
//	private Instance<ClassifierItemImport<?, ?>> classifierItemImports;
	
	@Inject @Any
	private Instance<ClassifierItemListImport> classifierItemListImports;	
	
	@Override
	public void launch(ProcessSignalContext context) {
		ControlSignal signal = context.getControlSignal();
		String resourceLabel = signal.getParameter(String.class, RESOURCE);
		Annotation ann = AnnotationInstanceProvider.of(Named.class, Map.of("value", resourceLabel));
		ClassifierItemListImport itemListImport = classifierItemListImports.select(ann).get();
		Language language = languageResolver.resolve(signal.getParameter(String.class, "language"));
		
		if (language.equals(DEFAULT_LANG)) {
			em = emProvider.lookup(null);
			itemListImport.firstPass(em, language);
		} else {
			em = emProvider.lookup(language.name());
			itemListImport.secondPass(em, language);
		}		
		
	}

}
