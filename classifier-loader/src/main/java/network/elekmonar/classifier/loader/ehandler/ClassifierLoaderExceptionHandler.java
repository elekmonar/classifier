package network.elekmonar.classifier.loader.ehandler;

import java.io.Serializable;

import javax.enterprise.event.Observes;
import javax.xml.bind.JAXBException;

import org.apache.deltaspike.core.api.exception.control.ExceptionHandler;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionEvent;

import network.elekmonar.commons.spec.exceptions.NoImportProcessException;

@ExceptionHandler
public class ClassifierLoaderExceptionHandler implements Serializable {

	private static final long serialVersionUID = 341380763885686187L;
	
	public void handleNoImportProcessException(@Observes ExceptionEvent<NoImportProcessException> event) {
		//event.getException().getResource()
	}
	
	public void handleJaxbException(@Observes ExceptionEvent<JAXBException> event) {
		
	}

	public void handleInstantiationException(@Observes ExceptionEvent<InstantiationException> event) {
		
	}

}