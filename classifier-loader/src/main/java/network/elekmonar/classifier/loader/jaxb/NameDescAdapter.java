package network.elekmonar.classifier.loader.jaxb;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import network.elekmonar.classifier.spi.beans.I18nProps;

public class NameDescAdapter extends XmlAdapter<AdapterMap, Map<String, I18nProps>> {

	@Override
	public Map<String, I18nProps> unmarshal(AdapterMap aMap) throws Exception {
        Map<String, I18nProps> map = new HashMap<>();
        for(Entry<?, ?> entry : aMap.entry) {
            map.put((String)entry.getKey(), (I18nProps)entry.getValue());
        }
        
        return map;
	}

	@Override
	public AdapterMap marshal(Map<String, I18nProps> v) throws Exception {
		return null;
	}

}