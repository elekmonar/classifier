package network.elekmonar.classifier.loader.starter;

import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.UnsatisfiedResolutionException;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.config.ConfigProperty;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.deltaspike.core.util.metadata.AnnotationInstanceProvider;
import org.hibernate.Session;

import network.elekmonar.classifier.persistence.SimpleClassifier;
import network.elekmonar.classifier.spi.XmlBeanTypesProvider;
import network.elekmonar.classifier.spi.beans.ClassifierI18nXmlBean;
import network.elekmonar.classifier.spi.beans.ClassifierXmlBean;
import network.elekmonar.classifier.spi.beans.I18nProps;
import network.elekmonar.classifier.spi.loader.XmlFileImportProcess;
import network.elekmonar.classifier.spi.persistence.Classifier;
import network.elekmonar.commons.spec.enums.Language;
import network.elekmonar.commons.spec.exceptions.NoImportProcessException;
import network.elekmonar.commons.utils.ChecksumHelper;
import network.elekmonar.elefante.jpa.EntityManagerProvider;
import network.elekmonar.elefante.jpa.EntityMetamodelProvider;
import network.elekmonar.libs.commons.annotations.HandlerFor;
import network.elekmonar.signal.spec.ControlSignal;
import network.elekmonar.signal.spec.spi.ProcessLauncher;
import network.elekmonar.signal.spec.spi.ProcessSignalContext;

@Named("classifier-xml-import")
public class ClassifierXmlImportProcessStarter implements ProcessLauncher, Serializable {

	private static final long serialVersionUID = -5879078155813983098L;
	
	private static final Language DEFAULT_LANG = Language.valueOf("RU");
	
	@Inject
	private EntityManagerProvider emProvider;
	
	private EntityManager em;
	
	@Inject @Any
	private Instance<XmlFileImportProcess<?, ?>> importProcesses;
	
	@Inject @ConfigProperty(name = "import.data.dir")
	private String importDataDir;
	
	@Inject @Any
	private Instance<XmlBeanTypesProvider> xmlBeanTypesProviders; 
	
	@Inject
	private Event<ExceptionToCatchEvent> exceptionToCatch;
	
	@Inject
	private EntityMetamodelProvider emp;
	
	@Inject
	private ChecksumHelper checksumHelper;
		
	private Path importDataPath;
	
	private Unmarshaller unmarshaller;
	
	private XmlFileImportProcess importProcess;
	
	private Map<String, String> checksums;
	
	private Language language;
	
	@PostConstruct
	private void doInit() {
		importDataPath = Paths.get(importDataDir);
	}
	
	public String capilatize(String str) {
		if (!str.contains("-")) {
			return StringUtils.capitalize(str);
		}
		
		String[] strs = str.split("-");
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < strs.length; i++) {
			sb.append(strs[i].substring(0,1).toUpperCase());
			sb.append(strs[i].substring(1));
		}
		
		return sb.toString();
	}
	
	@Override
	@SuppressWarnings({"unchecked"})	
	public void launch(ProcessSignalContext context) {		
		ControlSignal controlSingal = context.getControlSignal();
		String resourceLabel = controlSingal.getParameter(String.class, "resource");		
		String entityName = capilatize(resourceLabel);
		System.out.println("ENTITY.NAME = " + entityName);
		language = Language.valueOf(controlSingal.getParameter(String.class, "language").toUpperCase());
		
		Class<? extends SimpleClassifier> entityClass = (Class<? extends SimpleClassifier>)emp.getEntityClass(entityName);
		if (entityClass == null) {
			System.err.println("no entity");
			// TODO throw exception ...
		}
		
		Annotation ann = AnnotationInstanceProvider.of(HandlerFor.class, Map.of("value", entityClass));
		importProcess = null;		
		try {
			importProcess = importProcesses.select(ann).get();
		} catch(UnsatisfiedResolutionException e) {
			exceptionToCatch.fire(new ExceptionToCatchEvent(new NoImportProcessException(resourceLabel)));
		}

		
		Iterator<XmlBeanTypesProvider> xmlTypeIter = xmlBeanTypesProviders.iterator();
		List<Class<?>> xmlBeanTypes = new ArrayList<>();
		while (xmlTypeIter.hasNext()) {
			XmlBeanTypesProvider xmlTypeProvider = xmlTypeIter.next();
			xmlBeanTypes.addAll(xmlTypeProvider.getXmlBeans());			
		}
		Class<?>[] xbTypes = xmlBeanTypes.toArray(new Class[xmlBeanTypes.size()]);
		
		unmarshaller = null;
		try {
			JAXBContext jaxbCtx = JAXBContext.newInstance(xbTypes);
			unmarshaller = jaxbCtx.createUnmarshaller();			
		} catch (JAXBException e) {
			exceptionToCatch.fire(new ExceptionToCatchEvent(e));			
		}		
		
		if (language.equals(DEFAULT_LANG)) {
			em = emProvider.lookup(null);
			firstPass(resourceLabel, entityClass);
		} else {
			em = emProvider.lookup(language.name());
			secondPass(resourceLabel, entityClass);
		}		
		
	}
	
	private void firstPass(String resourceLabel, Class<? extends Classifier> entityClass) {		
		Session session = em.unwrap(Session.class);
		checksums = checksumHelper.loadFromDatabase(entityClass);
		Path resourceDir = Paths.get(importDataPath.toString(), resourceLabel);
		try (DirectoryStream<Path> ds = Files.newDirectoryStream(resourceDir)) {
			for (Path path : ds) {
				String checksum = checksumHelper.calculate(path);
				String fileName = path.getFileName().toString();
				String naturalId = fileName.substring(0, fileName.length()-4);
				boolean contains = false;
				if (checksums.containsKey(naturalId)) {					
					if (checksums.get(naturalId).equals(checksum)) {
						continue;
					}
					contains = true;
				}
				
				Classifier instance = null;
				if (contains) {
					instance = (Classifier)session.bySimpleNaturalId(entityClass).load(naturalId);
				} else {
					try {
						instance = entityClass.getConstructor().newInstance();
					} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
							| InvocationTargetException | NoSuchMethodException | SecurityException e) {
						exceptionToCatch.fire(new ExceptionToCatchEvent(e));
					} 
				}
				instance.setNaturalId(naturalId);
				
				ClassifierXmlBean xmlBean = (ClassifierXmlBean)unmarshaller.unmarshal(path.toFile());
				instance.setChecksum(checksum);
				ClassifierI18nXmlBean i18nXmlBean = (ClassifierI18nXmlBean)xmlBean;
				Classifier clrInstance = (Classifier)instance;
				Iterator<Entry<String, I18nProps>> iter = i18nXmlBean.getI18n().entrySet().iterator();
				while (iter.hasNext()) {					
					Entry<String, I18nProps> entry = iter.next();
					Language language = Language.valueOf(entry.getKey().toUpperCase());
					if (language.equals(DEFAULT_LANG)) {
						
						if (StringUtils.isNotEmpty(entry.getValue().getName())) {
							clrInstance.setName(entry.getValue().getName().trim());
						}
						if (StringUtils.isNotEmpty(entry.getValue().getDescription())) {
							clrInstance.setDescription(entry.getValue().getDescription().trim());
						}
						
						importProcess.doImportI18n(em, instance, entry.getValue());
					}
				}			
				
				importProcess.doImport(em, instance, xmlBean);				
				
				if (contains) {
					em.merge(instance);
				} else {
					instance.setNaturalId(naturalId);										
					em.persist(instance);
				}
				
			}
		} catch(IOException | JAXBException e) {
			exceptionToCatch.fire(new ExceptionToCatchEvent(e));
		}		
	}

	private void secondPass(String resourceLabel, Class<? extends Classifier> entityClass) {
		Session session = em.unwrap(Session.class);
		Path resourceDir = Paths.get(importDataPath.toString(), resourceLabel);
		try (DirectoryStream<Path> ds = Files.newDirectoryStream(resourceDir)) {
			for (Path path : ds) {
				String fileName = path.getFileName().toString();
				String naturalId = fileName.substring(0, fileName.length()-4);
				ClassifierXmlBean xmlBean = (ClassifierXmlBean)unmarshaller.unmarshal(path.toFile());
				ClassifierI18nXmlBean i18nXmlBean = (ClassifierI18nXmlBean)xmlBean;
				Classifier instance = (Classifier)session.bySimpleNaturalId(entityClass).load(naturalId);
				I18nProps i18nProps = i18nXmlBean.getI18n().get(language.name().toLowerCase());
								
				if (StringUtils.isNotEmpty(i18nProps.getName())) {
					instance.setName(i18nProps.getName().trim());
				}
				if (StringUtils.isNotEmpty(i18nProps.getDescription())) {
					instance.setDescription(i18nProps.getDescription().trim());
				}				
				
				importProcess.doImportI18n(em, instance, i18nProps);
				
				em.merge(instance);
			}
		} catch(IOException | JAXBException e) {
			exceptionToCatch.fire(new ExceptionToCatchEvent(e));
		}
	}
		
}