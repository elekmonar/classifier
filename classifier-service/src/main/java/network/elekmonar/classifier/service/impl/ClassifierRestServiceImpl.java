package network.elekmonar.classifier.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.config.ConfigProperty;
import org.apache.deltaspike.core.util.metadata.AnnotationInstanceProvider;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import network.elekmonar.classifier.persistence.I18nClassifier;
import network.elekmonar.classifier.service.ClassifierRestService;
import network.elekmonar.classifier.service.query.ClassifierQuerySelector;
import network.elekmonar.classifier.service.query.QuerySelectorCriteriaBean;
import network.elekmonar.classifier.service.reqbeans.ClassifierSearchReqBean;
import network.elekmonar.classifier.service.utils.ClassifierHashedRB;
import network.elekmonar.classifier.spi.beans.ClassifierRB;
import network.elekmonar.classifier.spi.persistence.Classifier;
import network.elekmonar.classifier.spi.transformer.RepresentationTransformer;
import network.elekmonar.commons.spec.dto.DataListDTO;
import network.elekmonar.commons.spec.dto.DataSelectionMeta;
import network.elekmonar.commons.spec.enums.Language;
import network.elekmonar.commons.utils.LanguageResolver;
import network.elekmonar.elefante.jpa.EntityMetamodelProvider;
import network.elekmonar.libs.commons.annotations.HandlerFor;
import network.elekmonar.libs.commons.exceptions.NoRequiredParameterException;
import network.elekmonar.modular.spec.identifier.ClassifierIdentifierManager;
import network.elekmonar.modular.spec.metamodel.MetamodelProvider;

@Stateless
public class ClassifierRestServiceImpl implements ClassifierRestService, Serializable {

	private static final long serialVersionUID = -1921751428314432139L;
	
	@EJB(lookup = "java:jboss/exported/identifier-app/identifier-impl/ClassifierIdentifierManagerImpl!network.elekmonar.modular.spec.identifier.ClassifierIdentifierManager")
	private ClassifierIdentifierManager identifierManager;

	@EJB(lookup = "java:jboss/exported/metamodel-app/metamodel-ejb/MetamodelProviderImpl!network.elekmonar.modular.spec.metamodel.MetamodelProvider")
	private MetamodelProvider metamodelProvider;
	
	@EJB
	private ClassifierQuerySelector querySelector;
	
	@Inject
	private EntityMetamodelProvider emp;
			
	@Inject @Any
	private Instance<RepresentationTransformer<?, ?>> reprTransformers;
	
	@Inject
	private LanguageResolver languageResolver;

	@Override
	public Response getMetainfo(String acceptType, String resourceLabel) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
		List<?> entityList = null;
		if (I18nClassifier.class.isAssignableFrom(entityClass)) {
			if (StringUtils.isEmpty(lang)) {
				//TODO throw Exception
			}
			Language language = Language.valueOf(lang.toUpperCase());
			Type[] types = ((ParameterizedType)transformer.getClass().getGenericSuperclass()).getActualTypeArguments();
			entityList = querySelector.list((Class<?>)types[0], resource, language);
		} else {
			entityList = querySelector.list(entityClass);
		}


 		List<? super ClassifierRB> reprList = new ArrayList<>();
		for (Object item : entityList) {
			SimpleRV instance = (SimpleRV)item;
			reprList.add(transformer.doTransform(instance));
		}

	 */

	@Override
	public Response getItems(String accept, String acceptLanguage, String resourceLabel, Integer pageSize, Integer page,
			String orderBy, String sortOrder, String lookupText, Boolean onlyNameLookup, String property, String value, String match, 
			String relationships, Boolean distinct, Boolean meta) {
		
		String resource = StringUtils.capitalize(resourceLabel);
		System.out.println("resource = " + resource);
		Class<?> entityClass = emp.getEntityClass(resource);
		System.out.println("entityClass = " + entityClass);
		Language language = languageResolver.resolve(acceptLanguage);
		
		QuerySelectorCriteriaBean criteriaBean = new QuerySelectorCriteriaBean();
		criteriaBean.setLanguage(language);
		criteriaBean.setPage(page);
		criteriaBean.setPageSize(pageSize);
		criteriaBean.setLookupText(lookupText);
		criteriaBean.setOnlyNameLookup(onlyNameLookup);
		criteriaBean.setOrderBy(orderBy);
		criteriaBean.setSortOrder(sortOrder);
		criteriaBean.setProperty(property);
		criteriaBean.setValue(value);
		criteriaBean.setRelationships(relationships);
		criteriaBean.setDistinct(distinct);		
		
		return processList(entityClass, criteriaBean, criteriaBean.getPage(), distinct, meta);		
	}
	
	@Override
	@SuppressWarnings({"unchecked", "rawtypes"})
	public Response getItem(String accept, String acceptLanguage, Long id) {
		System.out.println("== getItem acceptLanguage = " + acceptLanguage);
		Short resourceId = identifierManager.extractResourceId(id);
		System.out.println("resourceId = " + resourceId);
		String resource = metamodelProvider.getResourceLabelById(resourceId);
		System.out.println("resource = " + resource);
		
		Class<?> entityClass = emp.getEntityClass(resource);
		System.out.println("entityClass = " + entityClass);
		Annotation ann = AnnotationInstanceProvider.of(HandlerFor.class, Map.of("value", entityClass));
		RepresentationTransformer transformer = reprTransformers.select(ann).get();
				
		Language language = languageResolver.resolve(acceptLanguage);
		
		Classifier instance = null;
		if (I18nClassifier.class.isAssignableFrom(entityClass)) {
			Type[] types = ((ParameterizedType)transformer.getClass().getGenericSuperclass()).getActualTypeArguments();
			
			instance = (Classifier)querySelector.single((Class<?>)types[0], id, resource, language);
		} else {
			instance = (Classifier)querySelector.single(entityClass, id, language);
		}
				
		return Response
				.ok(transformer.doTransform(instance))
				.build();
	}
	
	@Override
	@SuppressWarnings({"unchecked", "rawtypes"})
	public Response getItemByExactPropertyMatch(String accept, String acceptLanguage, String resourceLabel, String property, String value) {
		Class<?> entityClass = emp.getEntityClass(StringUtils.capitalize(resourceLabel));
		
		Annotation ann = AnnotationInstanceProvider.of(HandlerFor.class, Map.of("value", entityClass));
		RepresentationTransformer transformer = reprTransformers.select(ann).get();
		
		Language language = languageResolver.resolve(acceptLanguage);

		Classifier instance = (Classifier)querySelector.singleByExactPropertyMatch(entityClass, property, value, language);
		return Response
				.ok(transformer.doTransform(instance))
				.build();
	}

	@Override
	public Response search(String accept, String acceptLanguage, InputStream is) {
		Language language = languageResolver.resolve(acceptLanguage);
		ObjectMapper mapper = new ObjectMapper();
		ClassifierSearchReqBean reqBean;
		try {
			reqBean = mapper.readValue(is, ClassifierSearchReqBean.class);
		} catch (IOException e) {
			throw new WebApplicationException(e);
		}
		
		if (reqBean.getResource() == null) {
			throw new WebApplicationException(new NoRequiredParameterException("resource"));
		}	
		
		String resource = StringUtils.capitalize(reqBean.getResource());
		System.out.println("resource = " + resource);
		Class<?> entityClass = emp.getEntityClass(resource);
		
		QuerySelectorCriteriaBean criteriaBean = new QuerySelectorCriteriaBean();
		criteriaBean.setLanguage(language);
		criteriaBean.setPage(reqBean.getPage());
		criteriaBean.setPageSize(reqBean.getPageSize());
		criteriaBean.setOrderBy(reqBean.getOrderBy());
		criteriaBean.setSortOrder(reqBean.getSortOrder());
		criteriaBean.setLookupText(reqBean.getLookupText());
		criteriaBean.setOnlyNameLookup(reqBean.getOnlyNameLookup());
		criteriaBean.setProperty(reqBean.getProperty());
		criteriaBean.setValue(reqBean.getValue());
		criteriaBean.setDistinct(reqBean.getDistinct());
		
		if (reqBean.getMeta() == null) {
			reqBean.setMeta(Boolean.TRUE);
		}
		
		return processList(entityClass, criteriaBean, criteriaBean.getPage(), reqBean.getDistinct(), reqBean.getMeta());
	}
	
	@SuppressWarnings({"unchecked", "rawtypes"})
	private Response processList(Class<?> entityClass, QuerySelectorCriteriaBean criteriaBean, Integer page, Boolean distinct, Boolean meta) {
		Annotation ann = AnnotationInstanceProvider.of(HandlerFor.class, Map.of("value", entityClass));
		RepresentationTransformer transformer = reprTransformers.select(ann).get();
		
		List<?> entityList = querySelector.list(entityClass, criteriaBean);
//		for (Object item : entityList) {
//			Classifier clr = (Classifier)item;
//			System.out.println("id = " + clr.getId() + ", name = " + clr.getName() + ", natural.id = " + clr.getNaturalId());
//		}
		
		final List<? super ClassifierRB> reprList;
		List<? super ClassifierRB> list = new ArrayList<>();
		for (Object item : entityList) {
			Classifier instance = (Classifier)item;
			list.add(transformer.doTransform(instance));
		}
		if (distinct == null || !distinct) {
			reprList = list;
		} else {
			reprList = distinct(list);
		}
		
		ObjectMapper mapper = new ObjectMapper();		
		mapper.setSerializationInclusion(Include.NON_NULL);
		StreamingOutput stream = new StreamingOutput() {
			
			@Override
			public void write(OutputStream output) throws IOException, WebApplicationException {
				if (meta.equals(Boolean.TRUE)) {
					Long count = querySelector.listCount(entityClass, criteriaBean);
					DataSelectionMeta dsMeta = new DataSelectionMeta(count, page);
					mapper.writeValue(output, new DataListDTO(reprList, dsMeta));	
				} else {
					mapper.writeValue(output, reprList);
				}				
			}
			
		};
		
		return Response
					.ok(stream)
					.build();
		
	}
	
	private List<? super ClassifierRB> distinct(List<? super ClassifierRB> reprList) {
		Set<ClassifierHashedRB> clrSet = new LinkedHashSet<>();
		
		for (Object clrRb : reprList) {
			ClassifierHashedRB clr = new ClassifierHashedRB((ClassifierRB)clrRb);
			clrSet.add(clr);
		}
		
		List<? super ClassifierRB> result = new ArrayList<>(clrSet);		
		
		return result;
	}
	
}