package network.elekmonar.classifier.service.reqbeans;

public class ClassifierSearchReqBean {
	
	private String resource;
	
	private String lookupText;
	
	private Boolean onlyNameLookup;
	
	private Integer page;
	
	private Integer pageSize;
	
	private String orderBy;
	
	private String sortOrder;
	
	private String property;
	
	private String value;
	
	private Boolean meta;
	
	private Boolean distinct;

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getLookupText() {
		return lookupText;
	}

	public void setLookupText(String lookupText) {
		this.lookupText = lookupText;
	}

	public Boolean getOnlyNameLookup() {
		return onlyNameLookup;
	}

	public void setOnlyNameLookup(Boolean onlyNameLookup) {
		this.onlyNameLookup = onlyNameLookup;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	
	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Boolean getMeta() {
		return meta;
	}

	public void setMeta(Boolean meta) {
		this.meta = meta;
	}

	public Boolean getDistinct() {
		return distinct;
	}

	public void setDistinct(Boolean distinct) {
		this.distinct = distinct;
	}
	
}