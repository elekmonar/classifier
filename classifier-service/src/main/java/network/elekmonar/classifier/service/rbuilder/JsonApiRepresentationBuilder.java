package network.elekmonar.classifier.service.rbuilder;

import java.io.Serializable;

import javax.inject.Named;

import network.elekmonar.classifier.service.spi.RepresentationBuilder;

@Named("application/vnd.api+json")
public class JsonApiRepresentationBuilder implements RepresentationBuilder, Serializable {

	private static final long serialVersionUID = 6104367783199442154L;

	@Override
	public void build() {
		
	}

}