package network.elekmonar.classifier.service.utils;

import network.elekmonar.classifier.spi.beans.ClassifierRB;

public class ClassifierHashedRB extends ClassifierRB {
	
	public ClassifierHashedRB(ClassifierRB classifierRB) {
		setId(classifierRB.getId());
		setNaturalId(classifierRB.getNaturalId());
		setName(classifierRB.getName());
		setDescription(classifierRB.getDescription());
	}

	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof ClassifierHashedRB)) {
            return false;
        }
        ClassifierHashedRB obj = (ClassifierHashedRB)value;

        if (getName() == null || obj.getName() == null) {
        	return false;
        }        
        
        return obj.getName().equals(getName());
    }

	@Override
    public int hashCode() {
        int result = 29;
        result += 29 * (getName() != null ? getName().hashCode() : 0);
        return result;
    }
	
}
