/**
 * Набор реализаций, определяющих переливку содержимого полей сущности 
 * в поля DTO-объекта, предназначенного для создания representation-слоя.
 */
package network.elekmonar.classifier.service.transformer;