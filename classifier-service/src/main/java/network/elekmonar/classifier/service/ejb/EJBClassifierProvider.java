package network.elekmonar.classifier.service.ejb;

import java.util.List;

import network.elekmonar.commons.spec.enums.Language;

public interface EJBClassifierProvider {
	
	public <T> T getItem(Class<T> beanClass, Long id);
	
	public <T> T getItem(Class<T> beanClass, Long id, Language language);
	
	public <T> List<T> getItems(Class<T> beanClass, List<Long> ids);
	
	public <T> List<T> getItems(Class<T> beanClass, List<Long> ids, Language language);

}