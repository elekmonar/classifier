package network.elekmonar.classifier.service.rbuilder;

import static javax.ws.rs.core.MediaType.APPLICATION_XML;

import java.io.Serializable;

import javax.inject.Named;

import network.elekmonar.classifier.service.spi.RepresentationBuilder;

@Named(APPLICATION_XML)
public class XmlRepresentationBuilder implements RepresentationBuilder, Serializable {

	private static final long serialVersionUID = 7561830019605552426L;

	@Override
	public void build() {
		// TODO Auto-generated method stub
	}

}