package network.elekmonar.classifier.service.query;

import network.elekmonar.commons.spec.enums.Language;

public class QuerySelectorCriteriaBean {
	
	private Class<?> entityClass;
	
	/**
	 * Match pattern
	 */
	private String lookupText;
	
	private Language language;
	
	private Integer page;
	
	private Integer pageSize;
	
	private Boolean onlyNameLookup;
	
	private String orderBy;
	
	private String sortOrder;
	
	private String property;	
	
	private String value;
	
	private String relationships;
	
	private Boolean distinct;
	
	public Class<?> getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(Class<?> entityClass) {
		this.entityClass = entityClass;
	}

	public String getLookupText() {
		return lookupText;
	}

	public void setLookupText(String lookupText) {
		this.lookupText = lookupText;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Boolean getOnlyNameLookup() {
		return onlyNameLookup;
	}

	public void setOnlyNameLookup(Boolean onlyNameLookup) {
		this.onlyNameLookup = onlyNameLookup;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}		

	public String getRelationships() {
		return relationships;
	}

	public void setRelationships(String relationships) {
		this.relationships = relationships;
	}

	public Boolean getDistinct() {
		return distinct;
	}

	public void setDistinct(Boolean distinct) {
		this.distinct = distinct;
	}
	
}