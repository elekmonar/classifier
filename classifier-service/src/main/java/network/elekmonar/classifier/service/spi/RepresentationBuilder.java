package network.elekmonar.classifier.service.spi;

public interface RepresentationBuilder {
	
	void build();

}