package network.elekmonar.classifier.service;

import java.io.InputStream;

import javax.ejb.Local;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import network.elekmonar.commons.spec.constants.Constants;

@Local
@Path("/classifiers")
@Produces(Constants.JSON_UTF8)
@Consumes(MediaType.APPLICATION_JSON)
public interface ClassifierRestService {

	/**
	 * Выдать метаинформацию по заданному типу справочника.
	 * 
	 * @param acceptType
	 * @param resourceLabel
	 * @return
	 */
    @HEAD
    @Path("/items/{label}")
    Response getMetainfo(
    		@HeaderParam("accept-type") String acceptType, 
    		@PathParam("label") String resourceLabel);
	
	
	/**
	 * Выдать полный перечень единиц заданного справочника.
	 * По умолчанию, длина списка ограничивается 1000 элементами.
	 * 
	 * 
	 * @param acceptType
	 * @param resourceLabel
	 * @return
	 */
    @GET
    @Path("/items")
    Response getItems(
    		@HeaderParam("Accept") String accept,
    		@HeaderParam("Accept-Language") String acceptLanguage,
    		@QueryParam("resource") String resourceLabel,
    		@QueryParam("pageSize") Integer pageSize,
    		@QueryParam("page") Integer page,    		
    		@QueryParam("orderBy") String orderBy,
    		@QueryParam("sortOrder") String sortOrder,
    		@QueryParam("lookupText") String lookupText,
    		@QueryParam("onlyNameLookup") @DefaultValue("true") Boolean onlyNameLookup,
    		@QueryParam("property") String property,
    		@QueryParam("value") String value,
    		@QueryParam("match") String match,
    		@QueryParam("relationships") String relationships,
    		@QueryParam("distinct") Boolean distinct,
    		@QueryParam("meta") @DefaultValue("true") Boolean meta);

    @GET
    @Path("/items/{label}/by/property/match")
    Response getItemByExactPropertyMatch(
    		@HeaderParam("Accept") String accept,
    		@HeaderParam("Accept-Language") String acceptLanguage,
    		@PathParam("label") String resourceLabel,
    		@QueryParam("property") String property,
    		@QueryParam("value") String value);

    @GET
    @Path("/items/{id}")
    Response getItem(
    		@HeaderParam("Accept") String accept, 
    		@HeaderParam("Accept-Language") String acceptLanguage,
    		@PathParam("id") Long id);
 
    @POST
    @Path("/search")
    Response search(
    		@HeaderParam("Accept") String accept,
    		@HeaderParam("Accept-Language") String acceptLanguage,
    		InputStream is);
    
}