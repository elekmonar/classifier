package network.elekmonar.classifier.service.rbuilder;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.io.Serializable;

import javax.inject.Named;

import network.elekmonar.classifier.service.spi.RepresentationBuilder;

@Named(APPLICATION_JSON)
public class JsonRepresentationBuilder implements RepresentationBuilder, Serializable {

	private static final long serialVersionUID = -4285702553023251686L;

	@Override
	public void build() {
		
	}

}