package network.elekmonar.classifier.service.impl;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.Map;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Any;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.util.metadata.AnnotationInstanceProvider;

import network.elekmonar.classifier.service.ClassifierLoaderService;
import network.elekmonar.commons.spec.annotations.ResourceTypeSpec;
import network.elekmonar.commons.spec.enums.Language;
import network.elekmonar.commons.spec.enums.ResourceType;
import network.elekmonar.commons.spec.events.ImportStartEvent;

@Stateless
public class ClassifierLoaderServiceImpl implements ClassifierLoaderService, Serializable {

	private static final long serialVersionUID = -9027036017069480481L;
	
	private static final Language DEFAULT_LANGUAGE = Language.valueOf("RU");
	
	@Inject @Any
	private Event<ImportStartEvent> startEvent;

	@Override
	@SuppressWarnings({"rawtypes", "unchecked"})
	public Response loadClassifier(String resource, String accessToken, String lang) {
		Language language;			
		if (StringUtils.isEmpty(lang)) {
			language = DEFAULT_LANGUAGE;
		} else {
			language = Language.valueOf(lang.toUpperCase());
		}
		Map payload = Map.of("resource", resource, "accessToken", accessToken, "language", language.name());
		
		Annotation ann = AnnotationInstanceProvider.of(ResourceTypeSpec.class, Map.of("value", ResourceType.RESOURCE));		
		startEvent.select(ann).fire(new ImportStartEvent(payload));
		return Response.noContent().build();
	}

	@Override
	@SuppressWarnings({"rawtypes", "unchecked"})
	public Response loadSet(Integer setId, String accessToken) {
		Map payload = Map.of("setId", setId, "accessToken", accessToken);
		Annotation ann = AnnotationInstanceProvider.of(ResourceTypeSpec.class, Map.of("value", ResourceType.INSTANCE_SET));
		startEvent.select(ann).fire(new ImportStartEvent(payload));
		return Response.noContent().build();
	}

}