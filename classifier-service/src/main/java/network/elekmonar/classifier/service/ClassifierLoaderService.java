package network.elekmonar.classifier.service;

import javax.ejb.Local;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import network.elekmonar.commons.spec.constants.Constants;

@Local
@Path("/classifiers")
@Produces(Constants.JSON_UTF8)
@Consumes(MediaType.APPLICATION_JSON)
public interface ClassifierLoaderService {
	
    @GET
    @Path("/loader/resource/{resource}")
    Response loadClassifier(
    		@PathParam("resource") String resource,
    		@QueryParam("token") String accessToken,
    		@QueryParam("lang") String lang);	

    @GET
    @Path("/loader/sets/{setId}")
    Response loadSet(
    		@PathParam("setId") Integer setId,
    		@QueryParam("token") String accessToken);	
    
}