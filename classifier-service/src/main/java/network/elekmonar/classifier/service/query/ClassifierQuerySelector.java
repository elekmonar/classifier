package network.elekmonar.classifier.service.query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.SingularAttribute;

import org.apache.commons.lang3.StringUtils;

import network.elekmonar.classifier.spi.persistence.Classifier;
import network.elekmonar.commons.spec.enums.Language;
import network.elekmonar.elefante.hibernate.dialect.ElefanteDialect;
//import network.elekmonar.iset.persistence.InstanceSet;
import network.elekmonar.elefante.jpa.EntityManagerProvider;

@Stateless
public class ClassifierQuerySelector implements Serializable {

	private static final long serialVersionUID = -154307183212681446L;
	
	@Inject
	private EntityManagerProvider emProvider;	
	
	public <T> T single(Class<T> entityClass, Long id, Language language) {
		EntityManager em = getEntityManager(language);
		return em.find(entityClass, id);
	}

	public <T> T single(Class<T> entityClass, Long id, String resourceLabel, Language language) {
		EntityManager em = getEntityManager(language);
		String queryName = resourceLabel.concat(".single");
		return em.createNamedQuery(queryName, entityClass)
				.setParameter(1, language.name())
				.setParameter(2, id)
				.getSingleResult();
	}
	
	public <T> T singleByExactPropertyMatch(Class<T> entityClass, String property, String value, Language language) {
		EntityManager em = getEntityManager(language);
		CriteriaBuilder cBuilder = em.getCriteriaBuilder();
		CriteriaQuery<T> cQuery = cBuilder.createQuery(entityClass);
		Root<T> cRoot = cQuery.from(entityClass);
		Predicate predicate = cBuilder.equal(cRoot.get(property), value);
		cQuery.where(predicate);
		try {
			return em.createQuery(cQuery).getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	public <T> Long listCount(Class<T> entityClass, QuerySelectorCriteriaBean criteriaBean) {
		EntityManager em = getEntityManager(criteriaBean.getLanguage());
		
		CriteriaBuilder cBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Long> cQuery = cBuilder.createQuery(Long.class);
		Root<T> cRoot = cQuery.from(entityClass);
		if (criteriaBean.getLookupText() != null) {
			Predicate predicate = getLookupPredicate(criteriaBean.getLookupText(), cBuilder, cQuery, cRoot, criteriaBean.getOnlyNameLookup());
			cQuery.where(predicate);			
		}
		cQuery.select(cBuilder.count(cRoot));
		
		return em.createQuery(cQuery).getSingleResult();
	}
	
	public <T> List<T> list(Class<T> entityClass, QuerySelectorCriteriaBean criteriaBean) {
		long t1 = System.currentTimeMillis();
		EntityManager em = getEntityManager(criteriaBean.getLanguage());
				
		CriteriaBuilder cBuilder = em.getCriteriaBuilder();
		CriteriaQuery<T> cQuery = cBuilder.createQuery(entityClass);
		Root<T> cRoot = cQuery.from(entityClass);
		List<Predicate> predicates = new ArrayList<>();
		
		if (StringUtils.isNotEmpty(criteriaBean.getProperty())) {
			Predicate predicate = cBuilder.equal(cRoot.get(criteriaBean.getProperty()), criteriaBean.getValue());
			predicates.add(predicate);
		} else if (StringUtils.isNotEmpty(criteriaBean.getLookupText())) {
			if (criteriaBean.getOnlyNameLookup() == null) {
				criteriaBean.setOnlyNameLookup(Boolean.TRUE);
			}			
			Predicate predicate = getLookupPredicate(criteriaBean.getLookupText(), cBuilder, cQuery, cRoot, criteriaBean.getOnlyNameLookup());
			predicates.add(predicate);
		}
		
		System.out.println("relationships = " + criteriaBean.getRelationships());
		if (StringUtils.isNotEmpty(criteriaBean.getRelationships())) {
			
			//String[] relationPairs = criteriaBean.getRelationships().split(",");
			String[] keyVal = criteriaBean.getRelationships().split(":");
			Path<?> relationPath = cRoot.get(keyVal[0]);
			Classifier relationship = (Classifier)em.getReference(relationPath.getJavaType(), Long.valueOf(keyVal[1]));
			Predicate predicate = cBuilder.equal(relationPath, relationship);
			predicates.add(predicate);
		}
				
		cQuery.where(predicates.toArray(new Predicate[predicates.size()]));
		
		if (criteriaBean.getOrderBy() == null) {
			criteriaBean.setOrderBy("name");
		}
		if (criteriaBean.getSortOrder() == null) {
			criteriaBean.setSortOrder("asc");
		}

		if (criteriaBean.getSortOrder().toLowerCase().equals("asc")) {
			cQuery.orderBy(cBuilder.asc(cRoot.get(criteriaBean.getOrderBy())));	
		} else {
			cQuery.orderBy(cBuilder.desc(cRoot.get(criteriaBean.getSortOrder())));
		}	
		
		TypedQuery<T> query = em.createQuery(cQuery);
		if (criteriaBean.getPageSize() != null) {
			query.setMaxResults(criteriaBean.getPageSize());	
		}
		if (criteriaBean.getPage() != null) {
			if (criteriaBean.getPage() < 0) {
				criteriaBean.setPage(0);
			}
			if (criteriaBean.getPageSize() == null) {
				criteriaBean.setPageSize(10);
			}
			query.setFirstResult(criteriaBean.getPage()*criteriaBean.getPageSize());	
		}
		
		List<T> resultList = query.getResultList();
		long t2 = System.currentTimeMillis();
		System.out.println("delta.time = " + (t2-t1));
		
		return resultList;
	}
	
	/**
	 * Выдать перечень элементов заданного ресурса (сущности).
	 * 
	 * @param <T>
	 * @param entityClass Класс сущности
	 * @return Список экземпляров сущностей БД.
	 */
	public <T> List<T> list(Class<T> entityClass, Language language) {
		EntityManager em = getEntityManager(language);
		CriteriaBuilder cBuilder = em.getCriteriaBuilder();
		CriteriaQuery<T> cQuery = cBuilder.createQuery(entityClass);
		Root<T> cRoot = cQuery.from(entityClass);
		
		return em.createQuery(cQuery).getResultList();
	}

	public <T> List<T> list(Class<T> entityClass, String resourceLabel, Language language) {
		EntityManager em = getEntityManager(language);
		String queryName = resourceLabel.concat(".all");
		List<T> list = em.createNamedQuery(queryName, entityClass)
				.setParameter(1, language.name())
				.getResultList();
		return list;
	}
	
	public <T> List<T> listBySetId(Class<T> entityClass, Integer setId) {
		/*
		InstanceSet instanceSet = em.find(InstanceSet.class, setId);
		Session session = em.unwrap(Session.class);
		return session
				.byMultipleIds(entityClass)
				.multiLoad(instanceSet.getIds());
		*/
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private <T> Predicate getLookupPredicate(String lookupValue, CriteriaBuilder cBuilder, CriteriaQuery<?> cQuery, Root<T> cRoot, boolean onlyName) {
		String lookupText = "^".concat(StringUtils.capitalize(lookupValue).trim());
		List<Predicate> predicates = new ArrayList<>();
		if (onlyName) {
			predicates.add(
		    		cBuilder.isTrue(
		    				cBuilder.function(
		    						ElefanteDialect.REGEXP_CASE_SENSITIVE, 
		    	                    Boolean.class,
		    	                    cRoot.get("name"),
		    	                    cBuilder.literal(lookupText)
		    	      		)
		    		)
			);			
		} else {
			for (Attribute<? super T, ?> attr : cRoot.getModel().getAttributes()) {
				if (attr.isAssociation()) {
					continue;
				}
				
				SingularAttribute<T, ?> singAttr = (SingularAttribute<T, ?>)attr;
				if (!singAttr.getJavaType().equals(String.class)) {
					continue;
				}
				
				predicates.add(
			    		cBuilder.isTrue(
			    				cBuilder.function(
			    						ElefanteDialect.REGEXP_CASE_SENSITIVE, 
			    	                    Boolean.class,
			    	                    cRoot.get(singAttr),
			    	                    cBuilder.literal(lookupText)
			    	      		)
			    		)
				);
			}			
		}
						
		return cBuilder.or(predicates.toArray(new Predicate[predicates.size()]));
	}	
	
	private EntityManager getEntityManager(Language language) {
		return emProvider.lookup(language.name().toLowerCase());
	}

}
