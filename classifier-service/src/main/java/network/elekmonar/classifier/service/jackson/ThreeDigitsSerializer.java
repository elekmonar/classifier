package network.elekmonar.classifier.service.jackson;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class ThreeDigitsSerializer extends JsonSerializer<Short> {

	@Override
	public void serialize(Short value, JsonGenerator gen, SerializerProvider serializers) throws IOException {	
		gen.writeString(String.format("%03d", value));
	}

}