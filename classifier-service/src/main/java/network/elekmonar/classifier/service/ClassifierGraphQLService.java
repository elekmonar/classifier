package network.elekmonar.classifier.service;

import java.io.InputStream;

import javax.ejb.Local;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import network.elekmonar.commons.spec.constants.Constants;

@Local
@Path("/classifiers")
//@Produces(HttpConstants.JSON_UTF8)
@Produces(Constants.JSON_UTF8)
@Consumes(MediaType.APPLICATION_JSON)
public interface ClassifierGraphQLService {

    @POST
    @Path("/graphql")
    Response getClassifierItems(InputStream is);
	
}