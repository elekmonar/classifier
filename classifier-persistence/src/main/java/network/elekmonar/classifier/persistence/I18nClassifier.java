package network.elekmonar.classifier.persistence;

import java.util.Map;

import network.elekmonar.commons.persistence.NameDesc;
import network.elekmonar.commons.spec.enums.Language;

public interface I18nClassifier {

	public Map<Language, NameDesc> getI18n();
	
	public void setI18n(Map<Language, NameDesc> i18n);

}