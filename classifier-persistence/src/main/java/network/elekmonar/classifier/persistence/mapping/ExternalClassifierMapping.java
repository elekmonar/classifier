package network.elekmonar.classifier.persistence.mapping;

public class ExternalClassifierMapping {
	
	private Long id;
	
	private ExternalResource externalResource;
	
	private Short resourceId;
	
	private String fromValue;
	
	private Long toValue;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	public ExternalResource getExternalResource() {
		return externalResource;
	}

	public void setExternalResource(ExternalResource externalResource) {
		this.externalResource = externalResource;
	}

	public Short getResourceId() {
		return resourceId;
	}

	public void setResourceId(Short resourceId) {
		this.resourceId = resourceId;
	}

	public String getFromValue() {
		return fromValue;
	}

	public void setFromValue(String fromValue) {
		this.fromValue = fromValue;
	}

	public Long getToValue() {
		return toValue;
	}

	public void setToValue(Long toValue) {
		this.toValue = toValue;
	}
	
}