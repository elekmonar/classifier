package network.elekmonar.classifier.persistence;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import network.elekmonar.classifier.spi.persistence.Classifier;

@MappedSuperclass
public class SimpleClassifier extends Classifier {

	private static final long serialVersionUID = 8420903895822532720L;
	
	private String name;
	
	private String description;

	@Column(name = "name", length = 64, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description", length = 1024, nullable = true)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}	

}