package network.elekmonar.classifier.persistence.mapping;

import java.io.Serializable;

public class ExternalResource implements Serializable {

	private static final long serialVersionUID = -8733367587287080078L;
	
	private Integer id;
	
	private String label;
	
	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}