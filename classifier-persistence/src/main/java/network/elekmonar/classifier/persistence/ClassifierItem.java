package network.elekmonar.classifier.persistence;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import network.elekmonar.elefante.hibernate.usertype.json.JsonbUserType;

@Entity(name = "ClassifierItem")
@Table(
		name = "classifier_item", 
		schema = "classifier"
)
@Inheritance(strategy = InheritanceType.JOINED)
@TypeDefs({
    @TypeDef(name = "jsonb", typeClass = JsonbUserType.class)
})
public class ClassifierItem implements Serializable {

	private static final long serialVersionUID = 4752106676502109700L;

	private Long id;
	
	private Short resourceId;
	
	private String naturalId;
	
	private LocalDateTime createdAt;
	
	private LocalDateTime modifiedAt;
	
	private String checksum;

	private Map<String, Object> props;

	@Id
	@Column(name = "id", nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "resource_id", nullable = false)
	public Short getResourceId() {
		return resourceId;
	}

	public void setResourceId(Short resourceId) {
		this.resourceId = resourceId;
	}

	@Column(name = "natural_id", nullable = false, length = 64)
	public String getNaturalId() {
		return naturalId;
	}

	public void setNaturalId(String naturalId) {
		this.naturalId = naturalId;
	}

	@Column(name = "created_at", nullable = false)
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "modified_at", nullable = false)
	public LocalDateTime getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(LocalDateTime modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	@Column(name = "checksum", nullable = false,  length = 64)
	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	
    @Type(type = "jsonb", parameters = {
            @Parameter(name = JsonbUserType.KEY_CLASS, value = "java.lang.String"),
            @Parameter(name = JsonbUserType.VALUE_CLASS, value = "java.lang.Object")
    })
    @Column(name = "props", nullable = false)		
	public Map<String, Object> getProps() {
		return props;
	}

	public void setProps(Map<String, Object> props) {
		this.props = props;
	}

	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof ClassifierItem)) {
            return false;
        }
        ClassifierItem obj = (ClassifierItem) value;

        if (getId() == null || obj.getId() == null) {
        	return false;
        }        
        
        return obj.getId().equals(getId());
    }

	@Override
    public int hashCode() {
        int result = 29;
        result += 29 * (getId() != null ? getId().hashCode() : 0);
        return result;
    }
	
}