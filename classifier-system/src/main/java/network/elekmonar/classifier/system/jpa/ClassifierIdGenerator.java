package network.elekmonar.classifier.system.jpa;

import java.io.Serializable;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;

import network.elekmonar.modular.spec.identifier.ClassifierIdentifierManager;


/**
 * Генератор идентификаторов сущностей.
 * 
 * @author Vitaly Masterov
 * @seee CurrentResourceIds
 * @since 0.1
 *
 */
public class ClassifierIdGenerator implements IdentifierGenerator, Configurable, Serializable {

	private static final long serialVersionUID = -3579045671800074778L;
	
	private static final String LOOKUP_PATH = "java:jboss/exported/identifier-app/identifier-impl/ClassifierIdentifierManagerImpl!network.elekmonar.modular.spec.identifier.ClassifierIdentifierManager";
	
	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		String resourceLabel = object.getClass().getSimpleName();
//		Class<?> entityClass = session.getEntityPersister(object.getClass().getName(), object).getClassMetadata().getMappedClass();
//		EntityManager em = session.getFactory().createEntityManager();
		
		try {
			Context ctx = new InitialContext();
			ClassifierIdentifierManager identifierManager = (ClassifierIdentifierManager)ctx.lookup(LOOKUP_PATH);
			Long identifier = identifierManager.nextClassifierId(resourceLabel);		
			return identifier;
		} catch (NamingException e) {
			// TODO !!!
			throw new RuntimeException(e);
		}		
	}	

	@Override
	public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
		
	}
	
}