package network.elekmonar.classifier.system.signal;

import java.io.Serializable;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;

import network.elekmonar.signal.spec.spi.SignalModuleListener;


/**
 * Приём и диспетчеризация JMS-сообщений от модуля Signal
 * 
 * @author Vitaly Masterov
 * @since 0.1
 * @see SignalModuleListener
 * @see MessageListener
 *
 */
@MessageDriven(name = "ClassifierSignalListener", activationConfig = {
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/app"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "AUTO_ACKNOWLEDGE"),
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "target = 'classifier'") 
})
public class ClassifierSignalListener implements MessageListener, Serializable {

	private static final long serialVersionUID = -6089833558239525087L;

    @EJB
    private SignalModuleListener delegate;
	
	@Override
	public void onMessage(Message message) {
		delegate.onMessage(message);
	}
	
}