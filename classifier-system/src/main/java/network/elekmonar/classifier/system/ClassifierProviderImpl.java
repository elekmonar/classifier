package network.elekmonar.classifier.system;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import network.elekmonar.classifier.spi.persistence.Classifier;
import network.elekmonar.classifier.spi.persistence.Classifier_;
import network.elekmonar.elefante.jpa.EntityManagerProvider;
import network.elekmonar.elefante.jpa.EntityMetamodelProvider;
import network.elekmonar.elefante.jpa.JPASelector;
import network.elekmonar.elefante.jpa.QueryBuilder;
import network.elekmonar.libs.commons.enums.SortingDirection;
import network.elekmonar.modular.spec.IdName;
import network.elekmonar.modular.spec.IdNameFactory;
import network.elekmonar.modular.spec.classifier.ClassifierProvider;
import network.elekmonar.modular.spec.identifier.ClassifierIdentifierManager;
import network.elekmonar.modular.spec.metamodel.MetamodelProvider;

@Stateless
public class ClassifierProviderImpl implements ClassifierProvider, Serializable {

	private static final long serialVersionUID = -2335475163158552223L;
	               
	@EJB(lookup = "java:jboss/exported/metamodel-app/metamodel-ejb/MetamodelProviderImpl!network.elekmonar.modular.spec.metamodel.MetamodelProvider")
	private MetamodelProvider metamodelProvider;

	@EJB(lookup = "java:jboss/exported/identifier-app/identifier-impl/ClassifierIdentifierManagerImpl!network.elekmonar.modular.spec.identifier.ClassifierIdentifierManager")
	private ClassifierIdentifierManager identifierManager;
	
	@Inject
	private JPASelector jpaSelector;
	
	@Inject
	private IdNameFactory idNameFactory;
	
	@Inject
	private EntityManagerProvider eManagerProvider;
	
	@Inject
	private EntityMetamodelProvider emProvider;

	@Override
	@SuppressWarnings({"unchecked", "rawtypes"})
	public List<IdName<Long>> getResultList(Short resourceId, String lookupText, String lang) {
		EntityManager em = eManagerProvider.lookup(lang);
		String entityName = metamodelProvider.getResourceLabelById(resourceId);
		Class entityClass = emProvider.getEntityClass(entityName);
		
		QueryBuilder qBuilder = new QueryBuilder<>(em, entityClass);
		qBuilder.lookupText(lookupText);
		qBuilder.orderBy(Classifier_.name, SortingDirection.ASC);
		List<Classifier> classifiers = qBuilder.list();
		
		List<IdName<Long>> result = new ArrayList<>();
		for (Classifier item : classifiers) {
			IdName<Long> idName = idNameFactory.newInstance(Long.class);
			idName.setId(item.getId());
			idName.setName(item.getName());
			result.add(idName);
		}
		
		return result;
	}

	@Override
	@SuppressWarnings({"unchecked", "rawtypes"})
	public IdName<Long> getSingleResult(Long id, String lang) {
		Short resourceId = identifierManager.extractResourceId(id);		
		EntityManager em = eManagerProvider.lookup(lang);
		String entityName = metamodelProvider.getResourceLabelById(resourceId);
		Class entityClass = emProvider.getEntityClass(entityName);				

		Classifier classifier = (Classifier)em.find(entityClass, id);
		
		IdName<Long> idName = idNameFactory.newInstance(Long.class);
		idName.setId(classifier.getId());
		idName.setName(classifier.getName());
		
		return idName;
	}

	@Override
	public <T> T getSingleResult(Class<T> beanClass, Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T getSingleResult(Class<T> beanClass, Long id, String language) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> List<T> getResultList(Class<T> beanClass, List<Long> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> List<T> getResultList(Class<T> beanClass, List<Long> ids, String language) {
		// TODO Auto-generated method stub
		return null;
	}

}